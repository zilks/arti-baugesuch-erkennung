import React, {useEffect, useState} from "react";
import {ScrollView, Text, View} from "react-native";
import {ActivityIndicator, Colors, DataTable} from "react-native-paper";

function ResultOverview(props) {
    const buildingApplicationResults = 'https://storage.googleapis.com/storage/v1/b/arti_baugesuch_final-output/o';

    const [resultData, setResultData] = useState({});
    const [isLoading, setIsLoading] = useState(true);

    async function getBuildingApplicationDetails(resultType) {
        try {
            console.log(props.globalType);

            if (props.globalType === 'success') {
                console.log(props.globalUser);
                console.log(props.globalAccessToken);

                return await fetch(resultType, {
                    headers: {Authorization: `Bearer ${props.globalAccessToken}`}
                }).then((response) => {
                    return response.json();
                }).then((data) => {
                    console.log(data);
                    setIsLoading(false);
                    setResultData(data);
                });
            } else {
                return { cancelled: true }
            }
        } catch {
            return { error: true }
        }
    }

    useEffect(() => {
        getBuildingApplicationDetails(buildingApplicationResults)
    }, []);

    if (isLoading) {
        return (
            <View style={{flex: 1, paddingTop: 250}}>
                <ActivityIndicator animating={true} color={Colors.grey500} />
            </View>
        );
    }

    return (
        <ScrollView>
            <Text>{resultData}</Text>
        </ScrollView>
    );
}

export default ResultOverview;