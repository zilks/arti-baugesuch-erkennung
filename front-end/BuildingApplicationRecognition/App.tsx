import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {
    Alert,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    StatusBar,
    Platform,
    Dimensions,
    FlatList,
} from 'react-native';
import {List, Provider as PaperProvider} from 'react-native-paper';
import { ActivityIndicator, Colors, DataTable } from 'react-native-paper';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { FontAwesome, Ionicons } from '@expo/vector-icons';
// @ts-ignore
import {NavigationContainer} from "@react-navigation/native";
// @ts-ignore
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Camera} from 'expo-camera';
import * as MediaLibrary from 'expo-media-library';
import * as Permissions from 'expo-permissions';
import * as Google from 'expo-google-app-auth';
import {createStackNavigator} from "@react-navigation/stack";

const Tab = createBottomTabNavigator();

const iosClientId = '368999536941-g8p365l4r2pe6k3tk4sbm05io2gpp48p.apps.googleusercontent.com';
const androidClientId = '368999536941-bjs8jf08afblstfn0ln7h8ue550vcffc.apps.googleusercontent.com';
const googleCloudStorageScope = 'https://www.googleapis.com/auth/devstorage.read_write';

let imageName = null;

let globalType = null;
let globalAccessToken = null;
let globalUser = null;

function CameraScreen() {
    const { width: winWidth, height: winHeight } = Dimensions.get('window');
    const [hasPermission, setHasPermission] = useState(null);
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [flashMode, setFlashMode] = useState(Camera.Constants.FlashMode.off);
    const [isLoading, setIsLoading] = useState(false);
    let cameraRollPermission = false;

    async function getCameraRollPermissions() {
        // @ts-ignore
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status === 'granted') {
            cameraRollPermission = true;
            console.log('Zugriff auf Fotos wurde gewährt.');
        } else {
            // Handle permissions denied;
            console.log('Zugriff auf Fotos wurde nicht gewährt.');
        }
    }

    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    if (hasPermission === null) {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>Kein Zugriff auf die Kamera</Text>;
    }

    async function takePicture() {
        if (this.camera) {
            await this.camera.takePictureAsync({ onPictureSaved: onPictureSaved });
            console.log('Bild wurde aufgenommen!');
        } else {
            Alert.alert('Kamera wird nicht erkannt!');
        }
    }

    async function onPictureSaved(photo) {
        console.log(photo.uri);

        await getCameraRollPermissions();
        if (cameraRollPermission) {
            console.log('Zugriff auf "Aufnahmen" ist gewährt.');
            await MediaLibrary.saveToLibraryAsync(photo.uri);
            console.log('Foto wurde zu den "Aufnahmen" hinzugefügt.');
        } else {
            Alert.alert('Zugriff auf die Foto-Aufnahmen ist nicht erlaubt.')
        }

        setIsLoading(true)

        await uploadTakenPicture(photo);
    }

    async function uploadTakenPicture(picture) {
        try {
            if (globalType === 'success') {
                console.log(globalUser);
                console.log(globalAccessToken);
                console.log(globalType);

                const fullDate = new Date();
                const year = fullDate.getFullYear();
                const month = fullDate.getMonth() + 1;
                const day = fullDate.getDate();
                const timeInMs = fullDate.getTime();
                const fileName = 'baugesuch-' + year + '-' + month + '-' + day + '-' + timeInMs + '.png';

                return await fetch(`https://storage.googleapis.com/upload/storage/v1/b/arti_baugesuch_image/o?uploadType=media&name=${fileName}`, {
                    headers: {
                        Authorization: `Bearer ${globalAccessToken}`,
                        'Content-Type': 'image/png'
                    },
                    method: 'POST',
                    body: picture
                }).then((response) => {
                    if (!response.ok) {
                        console.log('Status-Code:', response.status);
                        if (response.status === 403) {
                            Alert.alert('Sie haben keine Berechtigungen um Bilder hochzuladen!');
                            console.log('Sie haben keine Berechtigungen um Bilder hochzuladen!');
                            setIsLoading(false)
                            throw Error(response.statusText);
                        } else {
                            setIsLoading(false)
                            throw Error(response.statusText);
                        }
                    }
                    return response.json();
                }).then((data) => {
                    console.log(data);
                    setIsLoading(false)
                    Alert.alert('Foto wurde erfolgreich in die Google Cloud geladen.');
                }).catch((error) => {
                    console.error('Error:', error);
                    Alert.alert('Foto konnte nicht in die Google Cloud geladen werden!');
                });
            } else {
                return { cancelled: true }
            }
        } catch {
            return { error: true }
        }
    }

    if (isLoading) {
        return (
            <View style={{flex: 1, paddingTop: 250}}>
                <ActivityIndicator animating={true} color={Colors.grey500} />
            </View>
        );
    }

  return (
      <React.Fragment>
          <View>
              <Camera
                  type={type}
                  ref={ref => { this.camera = ref; }}
                  style={{height: winHeight, width: winWidth, position: 'absolute', left: 0, top: 0, right: 0, bottom: 0}}
              />
          </View>

          <Grid style={{width: winWidth, position: 'absolute', height: 100, bottom: 0}}>
              <Row>
                  <Col size={1} style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                      <TouchableOpacity
                          onPress={() => {
                              setType(
                                  type === Camera.Constants.Type.back
                                      ? Camera.Constants.Type.front
                                      : Camera.Constants.Type.back
                              );
                          }}>
                          <Ionicons
                              name="md-reverse-camera"
                              color="white"
                              size={30}
                          />
                      </TouchableOpacity>
                  </Col>
                  <Col size={2} style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                      <TouchableOpacity
                          onPress={() => {
                              takePicture();
                          }}>
                          <Text style={{
                              width: 60,
                              height: 60,
                              borderWidth: 2,
                              borderRadius: 30,
                              borderColor: "#FFFFFF" }}>
                          </Text>
                      </TouchableOpacity>
                  </Col>
                  <Col style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                      <TouchableOpacity onPress={() => {
                          setFlashMode(
                              flashMode === Camera.Constants.FlashMode.off
                                  ? Camera.Constants.FlashMode.on
                                  : Camera.Constants.FlashMode.off
                          );
                      }}>
                          <Ionicons
                              name={flashMode == Camera.Constants.FlashMode.on ? "md-flash" : 'md-flash-off'}
                              color="white"
                              size={30}
                          />
                      </TouchableOpacity>
                  </Col>
              </Row>
          </Grid>
      </React.Fragment>
  );
}

function ResultDetails() {
    const buildingApplicationObjectContent = `https://storage.googleapis.com/arti_baugesuch_final-output/${imageName}`;

    const [resultData, setResultData] = useState({});
    const [isLoading, setIsLoading] = useState(true);

    async function getBuildingApplicationDetails(resultType) {
        try {
            console.log('Details-Request Authentication: ' + globalType);

            if (globalType === 'success') {
                console.log(globalUser);
                console.log(globalAccessToken);
                console.log(imageName);
                console.log(buildingApplicationObjectContent);

                return await fetch(resultType, {
                    headers: {Authorization: `Bearer ${globalAccessToken}`}
                }).then((response) => {
                    return response.json();
                }).then((data) => {
                    console.log(data);
                    setIsLoading(false);
                    setResultData(data);
                });
            } else {
                return { cancelled: true }
            }
        } catch {
            return { error: true }
        }
    }

    useEffect(() => {
        getBuildingApplicationDetails(buildingApplicationObjectContent)
    }, []);

    if (isLoading) {
        return (
            <View style={{flex: 1, paddingTop: 250}}>
                <ActivityIndicator animating={true} color={Colors.grey500} />
            </View>
        );
    }

    return (
        <ScrollView>
            <DataTable>
                <DataTable.Header>
                    <DataTable.Title>Bezeichnung</DataTable.Title>
                    <DataTable.Title>Wert</DataTable.Title>
                </DataTable.Header>

                <DataTable.Row>
                    <DataTable.Cell>Adresse</DataTable.Cell>
                    <DataTable.Cell>{resultData.Adresse}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Auflagefrist bis</DataTable.Cell>
                    <DataTable.Cell>{resultData.AuflagefristBis}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Auflagefrist von</DataTable.Cell>
                    <DataTable.Cell>{resultData.AuflagefristVon}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Beschreibung</DataTable.Cell>
                    <DataTable.Cell>{resultData.Beschreibung}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Bezirk</DataTable.Cell>
                    <DataTable.Cell>{resultData.Bezirk}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Gemeinde</DataTable.Cell>
                    <DataTable.Cell>{resultData.Gemeinde}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Gemeinde Nr.</DataTable.Cell>
                    <DataTable.Cell>{resultData.GemeindeNr}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Gesuchsteller</DataTable.Cell>
                    <DataTable.Cell>{resultData.Gesuchsteller}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Kanton</DataTable.Cell>
                    <DataTable.Cell>{resultData.Kanton}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Land</DataTable.Cell>
                    <DataTable.Cell>{resultData.Land}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Ort</DataTable.Cell>
                    <DataTable.Cell>{resultData.Ort}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>PLZ</DataTable.Cell>
                    <DataTable.Cell>{resultData.PLZ}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Parzellen Nr.</DataTable.Cell>
                    <DataTable.Cell>{resultData.ParzellenNr}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Projekt ID</DataTable.Cell>
                    <DataTable.Cell>{resultData.ProjektID}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Projekverfasser</DataTable.Cell>
                    <DataTable.Cell>{resultData.Projekverfasser}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Sprache</DataTable.Cell>
                    <DataTable.Cell>{resultData.Sprache}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Stelle für Einsprache</DataTable.Cell>
                    <DataTable.Cell>{resultData.StelleFuerEinsprache}</DataTable.Cell>
                </DataTable.Row>
            </DataTable>
        </ScrollView>
    );
}

function ResultsOverview({ navigation }) {
    const buildingApplicationImages = 'https://storage.googleapis.com/storage/v1/b/arti_baugesuch_image/o';
    const buildingApplicationResults = 'https://storage.googleapis.com/storage/v1/b/arti_baugesuch_final-output/o';

    const [resultDataArray, setResultDataArray] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isRefreshing, setIsRefreshing] = useState(false);

    async function getBuildingApplicationDetails(resultType) {
        try {
            console.log(globalType);

            if (globalType === 'success') {
                console.log(globalUser);
                console.log(globalAccessToken);

                return await fetch(resultType, {
                    headers: {Authorization: `Bearer ${globalAccessToken}`}
                }).then((response) => {
                    return response.json();
                }).then((data) => {
                    console.log(data);
                    setIsLoading(false);
                    setIsRefreshing(false);
                    setResultDataArray(data.items);
                    console.log(resultDataArray);
                });
            } else {
                return { cancelled: true }
            }
        } catch {
            return { error: true }
        }
    }

    useEffect(() => {
        getBuildingApplicationDetails(buildingApplicationResults)
    }, []);

    if (isLoading) {
        return (
            <View style={{flex: 1, paddingTop: 250}}>
                <ActivityIndicator animating={true} color={Colors.grey500} />
            </View>
        );
    }

    return (
        <FlatList data={resultDataArray}
                  onRefresh={() => {
                      setIsRefreshing(true)
                      getBuildingApplicationDetails(buildingApplicationResults)
                  }}
                  refreshing={isRefreshing}
                  renderItem={({ item }) => (
                <List.Item
                    title={`${item.name}`}
                    description={`${item.bucket}`}
                    onPress={() => {
                        imageName = item.name;
                        console.log(imageName);
                        navigation.navigate('ResultDetails');
                    }}
                    left={props => <List.Icon {...props} icon="file-document" /> }
                />
            )}
        />
    );
}

const ResultsOverviewStack = createStackNavigator();

function ResultsOverviewScreen() {
    return (
        <ResultsOverviewStack.Navigator>
            <ResultsOverviewStack.Screen name={'ResultsOverview'} component={ResultsOverview} options={{ headerTitle: 'Resultate' }} />
            <ResultsOverviewStack.Screen name={'ResultDetails'} component={ResultDetails} options={{ headerTitle: 'Details' }} />
        </ResultsOverviewStack.Navigator>
    );
}


export default function App() {
    authenticateWithGoogle();

    async function authenticateWithGoogle() {
        try {
            // @ts-ignore
            const {type, accessToken, user} = await Google.logInAsync({
                iosClientId: `${iosClientId}`,
                androidClientId: `${androidClientId}`,
                scopes: [googleCloudStorageScope]
            });
            globalType = type;
            globalAccessToken = accessToken;
            globalUser = user;
        } catch {
            return {error: true}
        }
    }

  return (
      <PaperProvider>
          <View
              style={{
                  backgroundColor: 'white',
                  height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
              }}>
              <StatusBar
                  translucent
                  backgroundColor="white"
                  barStyle="dark-content"
              />
          </View>
          <NavigationContainer>
              <Tab.Navigator initialRouteName="CameraScreen"
                             screenOptions={({ route }) => ({
                                 tabBarIcon: ({ focused, color, size }) => {
                                     let iconName;

                                     if (route.name === 'Camera') {
                                         iconName = focused
                                             ? 'camera'
                                             : 'camera';
                                     } else if (route.name === 'Results') {
                                         iconName = focused ? 'list-alt' : 'list-alt';
                                     }
                                     return <FontAwesome name={iconName} size={size} color={color}/>;
                                 },
                             })}
                             tabBarOptions={{
                                 activeTintColor: 'cornflowerblue',
                                 inactiveTintColor: 'gray',
                             }}>
                  <Tab.Screen name="Camera" component={CameraScreen} options={{ tabBarLabel: 'Scanner' }}/>
                  <Tab.Screen name="Results" component={ResultsOverviewScreen} options={{ tabBarLabel: 'Resultate' }}/>
              </Tab.Navigator>
          </NavigationContainer>
      </PaperProvider>
  );
}